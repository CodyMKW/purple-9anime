# 9anime - Purple & grey theme by reddit user Kaito-Yagami
A userstyle to make the 9anime website purple and grey again!

![alt text](https://i.imgur.com/zYTexiP.png)

# How to install this theme
1. Install Stylus for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne),[Firefox](https://addons.mozilla.org/firefox/addon/styl-us/), or [Opera](https://addons.opera.com/en/extensions/details/stylus/). 
2. [![alt tag](https://img.shields.io/badge/Install%20directly%20with-Stylus-%233daee9?style=for-the-badge)](https://gitlab.com/CodyMKW/purple-9anime/-/raw/main/purple-9anime.user.css)
3. Click the “Install style” button on the page that opens.
![alt text](https://i.imgur.com/bS4isDz.png)
4. That's it nothing else needs to be done you can return to this page to update it or check for updates from within the extension. 
